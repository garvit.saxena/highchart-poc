"use client"
import { HighChartsWrapper } from '@/components/HOC/HighChartsWrapper'
import { Switch } from 'antd';
import { timeSeriesOptions, pieChartOptions, barChartOptions, singleBarChartOptions, areaMultipleSeriesChart, combineBarAndPieChart } from '../utils/chartOptions'
import { useState } from 'react';

export default function Home() {
  const [theme, setTheme] = useState<string>('light')

  const onThemeChange = (checked: boolean) => {
    setTheme(checked ? 'dark' : 'light')
    if (checked) {
      document.body.classList.add('dark-theme')
      document.body.classList.remove('light-theme')
    } else {
      document.body.classList.remove('dark-theme')
      document.body.classList.add('light-theme')
    }
  }

  return (
    <main className="min-h-screen p-12 bg-[#292827]">
      <div className='flex justify-end mb-8'>
        <Switch onChange={onThemeChange} />
      </div>
      <div className="grid grid-cols-3 gap-3">
        <HighChartsWrapper id={'chart-container-timeSeries'} options={timeSeriesOptions} theme={theme} height={300} />
        <HighChartsWrapper id={'chart-container-pieChart'} options={pieChartOptions} theme={theme} height={300} />
        <HighChartsWrapper id={'chart-container-barChart'} options={barChartOptions} theme={theme} height={300} />
        <HighChartsWrapper id={'chart-container-single-barChart'} options={singleBarChartOptions} theme={theme} height={300} />
        <HighChartsWrapper id={'chart-container-area-chart'} options={areaMultipleSeriesChart} theme={theme} height={300} />
        <HighChartsWrapper id={'chart-container-combine-bar-and-pieChart'} options={combineBarAndPieChart} theme={theme} height={300} />
      </div>
    </main>
  )
}
