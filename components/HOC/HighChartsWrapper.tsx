import dynamic from 'next/dynamic';
import React, { useEffect } from 'react'
import Highcharts from 'highcharts';
import { Options } from 'highcharts/highcharts'
import Exporting from 'highcharts/modules/exporting'
// import 'highcharts/css/highcharts.css'; // Import Highcharts CSS
import '../../styles/highchartsDark.scss'

Exporting(Highcharts);

export interface HighChartsWrapperProps {
    id: string
    options: Options | object
    theme: string
    width?: string | number
    height?: string | number
}

export const HighChartsWrapper: React.FC<HighChartsWrapperProps> = ({ id, options, theme, width, height }) => {
    useEffect(() => {
        Highcharts.chart(id, {
            ...options,
            chart: {
                // @ts-ignore
                ...options.chart,
                styledMode: theme === "dark"
            }
        });
    }, [id, options, theme]);

    return (
        <div id={id} style={{ width, height }}></div>
    )
}
